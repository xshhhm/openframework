/*
 * DPMFeatures.cpp
 *
 *  Created on: Mar 20, 2015
 *      Author: fds
 */

#include "DPMFeatures.h"
#include <cmath>

#include "DPMFunctions.h"


#define eps 0.0001

/*To determine the orientation*/

double uu[9] = {1.0000,
		0.9397,
		0.7660,
		0.500,
		0.1736,
		-0.1736,
		-0.5000,
		-0.7660,
		-0.9397};
double vv[9] = {0.0000,
		0.3420,
		0.6428,
		0.8660,
		0.9848,
		0.9848,
		0.8660,
		0.6428,
		0.3420};


static inline PRECISIONTYPE min(PRECISIONTYPE x, PRECISIONTYPE y) { return (x <= y ? x : y); }
static inline PRECISIONTYPE max(PRECISIONTYPE x, PRECISIONTYPE y) { return (x <= y ? y : x); }

static inline int min(int x, int y) { return (x <= y ? x : y); }
static inline int max(int x, int y) { return (x <= y ? y : x); }



DPMFeatures::DPMFeatures(const cv::Mat &Frame, int sbin, int padx, int pady): padx(padx), pady(pady) {
	//this->FullFeatures.resize(32);

//	std::cout << "sbin: " << sbin << std::endl;

//	std::cout << "We are going to calculate the DPM-features" << std::endl;

	/*Convert image to float?*/
	cv::Mat im_float;
	//Frame.convertTo(im_float, CV_32FC3);
	Frame.convertTo(im_float, CV_32FC3);

	if(Frame.channels() != 3){
		std::cerr << "We expect a 3-channel image for the pyramid!" << std::endl;
		exit(1);
	}

	int dims[3] = {Frame.rows, Frame.cols, Frame.channels()};

	int S[3];
//	std::cout << "Processing with sbin " << sbin << std::endl;
	double *feat = features(sbin, Frame.cols, Frame.rows, S, Frame);

	width = S[1];
	height = S[0];
	depth = S[2];


/*
	for(int j=0;j<10;j++){
	for(int i=0;i<10;i++){

			cv::Vec3f Yp1 = im_float.at<cv::Vec3f>(j, i);
			std::cout << Yp1.val[0] << "\t";
		}
		std::cout << std::endl;
	}
*/




/*
	  int blocks[2];
	  blocks[0] = (int)round(static_cast<float>(dims[0])/sbin);
	  blocks[1] = (int)round(static_cast<float>(dims[1])/sbin);

	  PRECISIONTYPE *hist = (PRECISIONTYPE *)calloc(blocks[0]*blocks[1]*18, sizeof(PRECISIONTYPE));
	  PRECISIONTYPE *norm = (PRECISIONTYPE *)calloc(blocks[0]*blocks[1], sizeof(PRECISIONTYPE));

	  int out[3];
	  out[0] = max(blocks[0]-2, 0);
	  out[1] = max(blocks[1]-2, 0);
	  out[2] = 27+4+1;


		this->width = out[1];
		this->height = out[0];
		this->depth = out[2];

	  //float *mxfeat = mxCreateNumericArray(3, out, mxDOUBLE_CLASS, mxREAL);
	  PRECISIONTYPE *feat = (PRECISIONTYPE *)calloc(out[0]*out[1]*out[2],sizeof(PRECISIONTYPE));

	  int visible[2];
	  visible[0] = blocks[0]*sbin;
	  visible[1] = blocks[1]*sbin;

	  // The HOG-features
	  for (int x = 1; x < visible[1]-1; x++) {
	      for (int y = 1; y < visible[0]-1; y++) {
	        // first color channel
	        //float *s = im_float. + min(x, dims[1]-2)*dims[0] + min(y, dims[0]-2);
	    	//  cv::Vec3f intensity;

	    	//float s = 0;
	    	//  im_float.at<cv::Vec3f>(y, x);

	    	  cv::Vec3f Yp1 = im_float.at<cv::Vec3f>(y+1, x);
	    	  cv::Vec3f Ym1 = im_float.at<cv::Vec3f>(y-1, x);

	    	  cv::Vec3f Xp1 = im_float.at<cv::Vec3f>(y, x+1);
	    	  cv::Vec3f Xm1 = im_float.at<cv::Vec3f>(y, x-1);


	    	 // Blue
	    	  PRECISIONTYPE dy = Yp1.val[0] - Ym1.val[0];
	    	  PRECISIONTYPE dx = Xp1.val[0] - Xm1.val[0];
	    	  PRECISIONTYPE v = dx*dx + dy*dy;

	        // Green
	        //s += dims[0]*dims[1];
	    	  PRECISIONTYPE dy2 = Yp1.val[1] - Ym1.val[1];
	    	  PRECISIONTYPE dx2 = Xp1.val[1] - Xm1.val[1];
	    	  PRECISIONTYPE v2 = dx2*dx2 + dy2*dy2;

	        // Red
	       // s += dims[0]*dims[1];
	    	  PRECISIONTYPE dy3 = Yp1.val[2] - Ym1.val[2];
	    	  PRECISIONTYPE dx3 = Xp1.val[2] - Xm1.val[2];
	    	  PRECISIONTYPE v3 = dx3*dx3 + dy3*dy3;

	    //    std::cout << dx << "  -  " << dx2 << "  -  " << dx3 << std::endl;

	        // pick channel with strongest gradient
	        if (v2 > v) {
	  	v = v2;
	  	dx = dx2;
	  	dy = dy2;
	        }
	        if (v3 > v) {
	  	v = v3;
	  	dx = dx3;
	  	dy = dy3;
	        }

	        // snap to one of 18 orientations
	        PRECISIONTYPE best_dot = 0;
	        int best_o = 0;
	        for (int o = 0; o < 9; o++) {
	        	PRECISIONTYPE dot = uu[o]*dx + vv[o]*dy;
	        //	std::cout << "dot: " << dot << std::endl;
	  	if (dot > best_dot) {
	  	  best_dot = dot;
	  	  best_o = o;
	  	} else if (-dot > best_dot) {
	  	  best_dot = -dot;
	  	  best_o = o+9;
	  	}
	   }

	        // add to 4 histograms around pixel using linear interpolation
	        PRECISIONTYPE xp = ((PRECISIONTYPE)x+0.5)/(PRECISIONTYPE)sbin - 0.5;
	        PRECISIONTYPE yp = ((PRECISIONTYPE)y+0.5)/(PRECISIONTYPE)sbin - 0.5;
	        int ixp = (int)floor(xp);
	        int iyp = (int)floor(yp);
	        PRECISIONTYPE vx0 = xp-ixp;
	        PRECISIONTYPE vy0 = yp-iyp;
	        PRECISIONTYPE vx1 = 1.0-vx0;
	        PRECISIONTYPE vy1 = 1.0-vy0;
	        v = sqrt(v);

	        if (ixp >= 0 && iyp >= 0) {
	        		*(hist + ixp*blocks[0] + iyp + best_o*blocks[0]*blocks[1]) +=   vx1*vy1*v;
	        }

	        if (ixp+1 < blocks[1] && iyp >= 0) {
	        		*(hist + (ixp+1)*blocks[0] + iyp + best_o*blocks[0]*blocks[1]) += vx0*vy1*v;
	        }

	        if (ixp >= 0 && iyp+1 < blocks[0]) {
	  	*(hist + ixp*blocks[0] + (iyp+1) + best_o*blocks[0]*blocks[1]) +=  vx1*vy0*v;
	        }

	        if (ixp+1 < blocks[1] && iyp+1 < blocks[0]) {
	  	*(hist + (ixp+1)*blocks[0] + (iyp+1) + best_o*blocks[0]*blocks[1]) +=  vx0*vy0*v;
	        }
	      }
	    }


	  // compute energy in each block by summing over orientations
	  for (int o = 0; o < 9; o++) {
		  PRECISIONTYPE *src1 = hist + o*blocks[0]*blocks[1];
		  PRECISIONTYPE *src2 = hist + (o+9)*blocks[0]*blocks[1];
		  PRECISIONTYPE *dst = norm;
		  PRECISIONTYPE *end = norm + blocks[1]*blocks[0];
	    while (dst < end) {
	      *(dst++) += (*src1 + *src2) * (*src1 + *src2);
	      src1++;
	      src2++;
	    }
	  }



	  // compute features
	  for (int x = 0; x < out[1]; x++) {
	    for (int y = 0; y < out[0]; y++) {
	    	PRECISIONTYPE *dst = feat + x*out[0] + y;
	    	PRECISIONTYPE *src, *p, n1, n2, n3, n4;

	      p = norm + (x+1)*blocks[0] + y+1;
	     // std::cout << "p: " << *p << std::endl;
	      n1 = 1.0 / sqrt(*p + *(p+1) + *(p+blocks[0]) + *(p+blocks[0]+1) + eps);
	    //  std::cout << "n1: " << n1 << std::endl;
	      p = norm + (x+1)*blocks[0] + y;
	      n2 = 1.0 / sqrt(*p + *(p+1) + *(p+blocks[0]) + *(p+blocks[0]+1) + eps);
	      p = norm + x*blocks[0] + y+1;
	      n3 = 1.0 / sqrt(*p + *(p+1) + *(p+blocks[0]) + *(p+blocks[0]+1) + eps);
	      p = norm + x*blocks[0] + y;
	      n4 = 1.0 / sqrt(*p + *(p+1) + *(p+blocks[0]) + *(p+blocks[0]+1) + eps);

	      PRECISIONTYPE t1 = 0;
	      PRECISIONTYPE t2 = 0;
	      PRECISIONTYPE t3 = 0;
	      PRECISIONTYPE t4 = 0;

	      // contrast-sensitive features
	      src = hist + (x+1)*blocks[0] + (y+1);
	      for (int o = 0; o < 18; o++) {
	    	  PRECISIONTYPE h1 = min(*src * n1, 0.2);
	    	  PRECISIONTYPE h2 = min(*src * n2, 0.2);
	    	  PRECISIONTYPE h3 = min(*src * n3, 0.2);
	    	  PRECISIONTYPE h4 = min(*src * n4, 0.2);
		*dst = 0.5 * (h1 + h2 + h3 + h4);
		t1 += h1;
		t2 += h2;
		t3 += h3;
		t4 += h4;
		dst += out[0]*out[1];
		src += blocks[0]*blocks[1];
	      }

	      // contrast-insensitive features
	      src = hist + (x+1)*blocks[0] + (y+1);
	      for (int o = 0; o < 9; o++) {
	    	  PRECISIONTYPE sum = *src + *(src + 9*blocks[0]*blocks[1]);
	    	  PRECISIONTYPE h1 = min(sum * n1, 0.2);
	    	  PRECISIONTYPE h2 = min(sum * n2, 0.2);
	    	  PRECISIONTYPE h3 = min(sum * n3, 0.2);
	    	  PRECISIONTYPE h4 = min(sum * n4, 0.2);
	        *dst = 0.5 * (h1 + h2 + h3 + h4);
	        dst += out[0]*out[1];
	        src += blocks[0]*blocks[1];
	      }

	      // texture features
	      *dst = 0.2357 * t1;
	      dst += out[0]*out[1];
	      *dst = 0.2357 * t2;
	      dst += out[0]*out[1];
	      *dst = 0.2357 * t3;
	      dst += out[0]*out[1];
	      *dst = 0.2357 * t4;

	      // truncation feature
	      dst += out[0]*out[1];
	      *dst = 0;
	    }
	  }

	free(hist);
	free(norm);
*/
// Pad the features
int pad[2] = {pady+1, padx+1};
int size[] = {height, width, depth};
//std::cout << "Size: " << size[0] << "x" << size[1] << "x" << size[2] << std::endl; 
feat = padarray(feat, pad, size);

//std::cout << "Size: " << size[0] << "x" << size[1] << "x" << size[2] << std::endl; 

width = size[1];
height = size[0];
depth = size[2];

int place=31;

for(int W=0;W<=pady;W++){
	for(int Q=0;Q<width;Q++){
        	feat[place*width*height+ Q * height + W ] = 1;
        }
}
for(int W=height-pady-1;W<height;W++){
	for(int Q=0;Q<width;Q++){
        	feat[place*height*width+ Q * height + W ] = 1;
        }
}

for(int W=0;W<height;W++){
	for(int Q=0;Q<=padx;Q++){
        	feat[place*height*width+ Q * height + W ] = 1;
        }
}

for(int W=0;W<height;W++){
	for(int Q=width-padx-1;Q<width;Q++){
        	feat[place*width*height+ Q * height + W ] = 1;
        }
}


	this->FullFeatures = feat;
}

DPMFeatures::~DPMFeatures() {
	free(FullFeatures);
}

