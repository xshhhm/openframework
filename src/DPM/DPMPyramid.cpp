/*
 * DPMPyramid.cpp
 *
 *  Created on: Mar 20, 2015
 *      Author: fds
 */

#include "DPMPyramid.h"

#include <iostream>


int amin(int i[2],int n){
        if(i[0]<i[1])
                return i[0];
        return i[1];
}


DPMPyramid::DPMPyramid(const cv::Mat &Frame, int padx, int pady, int interval):padx(padx), pady(pady), interval(interval) {
//	std::cout << "Start with the DPM pyramid" << std::endl;
	
	/*Standard sbin for DPM*/
	int sbin = 8;

	/*The default rescaling (interval gives the amount of layers per octave)*/
	double sc = pow(2.0,(1.0/interval));


	int imsize[2] = {Frame.rows, Frame.cols};
	int max_scale = 1 + floor(log(amin(imsize,2)/(5.0*sbin))/log(sc));
	
	/*Amount of layers*/
	int lenghtFeatures = max_scale+interval;

	/*Now we know the size of the pyramid*/
	scales.resize(lenghtFeatures);
	sbins.resize(lenghtFeatures);
	imscale.resize(lenghtFeatures);
	//fill in the scales and sbins 
	for(int i=0;i<interval;i++){
		scales[i] = 2.0/std::pow(sc,(i));
//		std::cout << "IMRESCALE: " <<  1.0/std::pow(sc,(i)) << std::endl;
		imscale[i] = 1.0/std::pow(sc,(i));
		sbins[i] = sbin/2; //calculated with smaller sbin instead of upscaling the image
		
		scales[i+interval] = 1.0/std::pow(sc,(i));
		imscale[i+interval] = 1.0/std::pow(sc,(i));
		sbins[i+interval] = sbin; //calculated with smaller sbin instead of upscaling the image
		for(int j=i+interval;j<max_scale;j+=interval){
			imscale[j+interval] = 0.5*scales[j];
//			std::cout << "IMSCALE1: " << imscale[j+interval] << "in "  << j+interval << std::endl;
			scales[j+interval] = 0.5*scales[j];
			sbins[j+interval] = sbin;
		}
	}

	// option for parallelisation
	for(int s=0;s<scales.size();s++){
		cv::Mat Scaled;
//		std::cout << "Rescaling with " << imscale[s] << std::endl;
		cv::resize(Frame,Scaled,cv::Size(),imscale[s],imscale[s]);
		this->Features.push_back(new DPMFeatures(Scaled,sbins[s], padx, pady));
	}



	//Features[Features.size()-1]->PrintLayer();

}

DPMPyramid::~DPMPyramid() {
	//delete the features
	for(int l=0;l<Features.size();l++){
		delete Features[l];
	}
}

