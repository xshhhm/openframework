//============================================================================
// Author      : F. De Smedt @ EAVISE
// Copyright   : This code is written for the publication of  "Open Framework for Combined Pedestrian detection". This code is for research and educational purposes only. For a different license, please contact the contributors directly. More information about the license can be fount in license.txt
//============================================================================

#include "Combinator.h"
#include "dpmdetectorOld.h"
#include "dpmdetector.h"
#include "chnftrsdetector.h"

#include "helper.hpp"
#include "HasOverlap.h"

/*Convert an integer to a string*/
std::string I2Str(int index){
	std::stringstream SS;
	SS << index;
	return SS.str();
}


template <typename Graph, typename NameMap>
inline std::map<std::string, typename boost::graph_traits<Graph>::vertex_descriptor>
AddCouple(Graph& g, NameMap nm, std::string index1, std::string index2)
{
    typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
    std::map<std::string, Vertex> verts;
    Vertex u = add_named_vertex(g, nm, index1, verts);
    Vertex v = add_named_vertex(g, nm, index2, verts);
    add_edge(u, v, g);

    return verts;
}


std::vector<std::vector<DetectionNode> > GroupDetections(std::vector<DetectionNode> &Nodes){
	std::vector<bool> Found(Nodes.size(),false);
	std::vector<std::vector<DetectionNode> > Groups;
	std::vector<std::vector<int> > Indeces;

	Graph g;
	NameMap nm(get(&Actor::name, g));

	clique_adder vis(Indeces);

	// look for "same detections"
	for(int d=0;d<Nodes.size();d++){
		bool foundmatch=false;
		for(int d2=d+1;d2<Nodes.size();d2++){
			if(HasOverlap(Nodes[d].Det, Nodes[d2].Det) >= 0.5){
				AddCouple(g,nm,I2Str(d),I2Str(d2));
				Found[d] =true;
				Found[d2] =true;
			}
		}
	}

	// Do the actual search for groups
	bron_kerbosch_all_cliques(g, vis);

	// add the "groups" to return them
	for(int I=0;I<Indeces.size();I++){
		std::vector<DetectionNode> Pair;
		for(int I2=0;I2<Indeces[I].size();I2++)
			Pair.push_back(Nodes[Indeces[I][I2]]);
		Groups.push_back(Pair);
	}

	// Add the single detections as a separate group
	for(int l=0;l<Found.size();l++){
		if(!Found[l]){
			std::vector<DetectionNode> Single;
			Single.push_back(Nodes[l]);
			Groups.push_back(Single);

		}
	}

	return Groups;
}

Combinator::Combinator() {
	this->Detectors.push_back(new DPMDetectorOld());

	this->Detectors.push_back(new ChnFtrsDetector());

	// Perform The Combinator instead of Pool-Combination
	this->doCombinator = true;
}

Combinator::~Combinator() {
	// TODO Auto-generated destructor stub
}


DetectionList Combinator::applyCombinator(const cv::Mat &Frame) const{
	DetectionList DL;

	NonMaximumSuppression NMS;

	std::vector<Detection*> Ds;
	std::vector<float> confidences;
			std::vector<float> complementarities;
			std::vector<DetectionNode> DNodes;

			//retrieve Confidence- and complementarity-values
			for(int D=0;D<this->Detectors.size();D++){
				confidences.push_back(Detectors[D]->getConfidence());
				complementarities.push_back(Detectors[D]->getComplementarity());
			}


		// obtain NMS-detections of detectors
			for(int D=0;D<this->Detectors.size();D++){
				//Run Detector
				DetectionList D1 = this->Detectors[D]->applyDetector(Frame);
				// Normalise scores
				D1.normaliseScore(Detectors[D]->getMean(), Detectors[D]->getStd());

				// Filter "same detections" per detector
				DetectionList NonMax = NMS.standardNMS(D1);

				// Add as a detection node in the graph structure
				for(int D2=0;D2<NonMax.getSize();D2++){
					DNodes.push_back(DetectionNode(NonMax.Ds[D2], D, D2));
				}
			}

		// Group Detections, each group contains detections of "same object"
			std::vector<std::vector<DetectionNode> > Groups = GroupDetections(DNodes);

		// Combine each group to a single detection
			//to make sure scores are positive in summation
			float offset = 500;
			float complementarity = 1; // for pair-wise combinations, the complementarity has no additional value

			for(int G=0;G<Groups.size();G++){
				float score = 0;
				float X=0,Y=0,W=0,H=0;

				int counter=0;
				for(int G1=0;G1<Groups[G].size();G1++){
					Detection *DD = Groups[G][G1].Det;
					score += (DD->getScore()+offset) * confidences[Groups[G][G1].DetectorIndex] * complementarity;
					X+=DD->getX();
					Y+=DD->getY();
					W+=DD->getWidth();
					H+=DD->getHeight();
					counter++;
				}
				Ds.push_back(new Detection(X/counter, Y/counter, W/counter, H/counter,score));
			}

			// Add the detections to a DetectionList to return
			for(int DD=0;DD<Ds.size();DD++){
				DL.addDetection(Ds[DD]->getX(),Ds[DD]->getY(),Ds[DD]->getWidth(),Ds[DD]->getHeight(),Ds[DD]->getScore());
				delete Ds[DD];
			}

	return DL;
}

DetectionList Combinator::applyPoolCombination(const cv::Mat &Frame) const{
	DetectionList DL;

	for(int d=0;d<this->Detectors.size();d++){
		// Perform detections
		DetectionList D1 = Detectors[d]->applyDetector(Frame);
		/*Normalise the scores*/
		D1.normaliseScore(Detectors[d]->getMean(), Detectors[d]->getStd());
		/*Add the detections to the list*/
		DL.addList(D1);
	}

	return DL;
}


DetectionList Combinator::applyDetector(const cv::Mat &Frame) const{

			if(this->doCombinator){
				/*Perform the combination technique of The Combinator*/
				return this->applyCombinator(Frame);
			}
			else{
				/*Perform Pool Combination*/
				return this->applyPoolCombination(Frame);
			}
}
