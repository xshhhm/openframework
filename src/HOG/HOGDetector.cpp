#include "HOGDetector.h"

using namespace cv;

HOGDetector::HOGDetector(){
	std::cout << "Initialise HOG Detector" << std::endl;

    hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());

}


DetectionList HOGDetector::applyDetector(const cv::Mat &Frame) const{
DetectionList DL;

	float Upscale = 1.0;

	cv::Mat U;
	cv::resize(Frame,U,cv::Size(),Upscale,Upscale);

// HOG
	std::vector<cv::Rect> found, found_filtered;
	std::vector<double> Scores, Scores_filtered;

	//hog.detectMultiScale(Frame, found, 0, Size(8,8), Size(32,32), 1.05, 2);
	//hog.detectMultiScale(Frame, found, Scores, 0, Size(8,8), Size(32,32), 1.05, 2, true);
	hog.detectMultiScale(U, found, Scores, 0, Size(8,8), Size(32,32), 1.05, 0);
	

//void HOGDescriptor::detectMultiScale(const Mat& img, vector<Rect>& foundLocations, vector<double>& foundWeights, double hitThreshold, Size winStride, Size padding, double scale0, double finalThreshold, bool useMeanshiftGrouping)

/*
	std::cout << "SCORES: " << Scores.size() << std::endl;
	for(int i=0;i<Scores.size();i++)
		std::cout << Scores[i] << std::endl;
*/
/*
	size_t i, j;
        for( i = 0; i < found.size(); i++ )
        {
            cv::Rect r = found[i];
            for( j = 0; j < found.size(); j++ )
                if( j != i && (r & found[j]) == r)
                    break;
            if( j == found.size() ){
                found_filtered.push_back(r);
	    	Scores_filtered.push_back(Scores[i]);
		}
        }*/
        for(int i = 0; i < found.size(); i++ )
        {
            Rect r = found[i];
            // the HOG detector returns slightly larger rectangles than the real objects.
            // so we slightly shrink the rectangles to get a nicer output.
            r.x += cvRound(r.width*0.1);
            r.width = cvRound(r.width*0.8);
            r.y += cvRound(r.height*0.07);
            r.height = cvRound(r.height*0.8);
       //     rectangle(Frame, r.tl(), r.br(), cv::Scalar(0,255,0), 3);
        
//		std::cout << "Detection: " << r << "  width score " << Scores_filtered[i] << std::endl;	
		DL.addDetection(r.x,r.y,r.width,r.height,Scores[i]);
	}



DL.resizeDetections(Upscale);

return DL;
}
